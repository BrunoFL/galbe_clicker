/* Fait par Bruno le 9/08/2017 */

var galbe = multiplicateur = 1, time_log = Date.now();
document.getElementById('zone').addEventListener('click', muscle);
setInterval(draw_galbe, 100);

document.getElementById('prot').addEventListener('click', bonus);

document.getElementById('buy1').addEventListener('click', multi2);
document.getElementById('buy2').addEventListener('click', multi10);
document.getElementById('buy3').addEventListener('click', multi100);
document.getElementById('buy4').addEventListener('click', multi400);

function draw() {
    var canvas = document.getElementById('canvas');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var img = document.getElementById("bicepsImg");
        ctx.drawImage(img, 10, 10);
        ctx.ellipse(130, 125, 50, 15 + 3.5 * Math.log2(galbe), 0, 0,
                    Math.PI * 2);
        ctx.fillStyle = '#F3D5AB';
        ctx.fill();
        ctx.lineWidth = 5;
        ctx.strokeStyle = 'black';
        ctx.stroke();
    }
    }

function muscle() {
    galbe += multiplicateur;

    if (galbe >= 20) {
        var el = document.getElementById('buy1');
        var disp = getComputedStyle(el, null).display;
        if (disp == 'none') {
            el.style.display = 'block';
        }
        }

    if (galbe >= 500) {
        var el = document.getElementById('buy2');
        var disp = getComputedStyle(el, null).display;
        if (disp == 'none') {
            el.style.display = 'block';
        }
        }

    if (galbe >= 8000) {
        var el = document.getElementById('buy3');
        var disp = getComputedStyle(el, null).display;
        if (disp == 'none') {
            el.style.display = 'block';
        }
        }

    if (galbe >= 120000) {
        var el = document.getElementById('buy4');
        var disp = getComputedStyle(el, null).display;
        if (disp == 'none') {
            el.style.display = 'block';
        }
    }
    }

function draw_galbe() {
    document.getElementById('galbePTS').innerHTML = parseInt(galbe);
    draw();

    if (Date.now() > time_log + 27000) {
        document.getElementById('prot').style.display = "block";
        time_log = Date.now();
    }
    }

function multi2() {
    var prix = parseInt(document.getElementById('prix1').innerHTML);
    if (galbe >= prix) {
        galbe -= prix;
        multiplicateur += 2;
        document.getElementById('prix1').innerHTML = Math.floor(prix * 1.5);
    }
    }

function multi10() {
    var prix = parseInt(document.getElementById('prix2').innerHTML);
    if (galbe >= prix) {
        galbe -= prix;
        multiplicateur += 10;
        document.getElementById('prix2').innerHTML = Math.floor(prix * 1.3);
    }
    }

function multi100() {
    var prix = parseInt(document.getElementById('prix3').innerHTML);
    if (galbe >= prix) {
        galbe -= prix;
        multiplicateur += 100;
        document.getElementById('prix3').innerHTML = Math.floor(prix * 1.2);
    }
    }

function multi400() {
    var prix = parseInt(document.getElementById('prix4').innerHTML);
    if (galbe >= prix) {
        galbe -= prix;
        multiplicateur += 400;
        document.getElementById('prix4').innerHTML = Math.floor(prix * 1.17);
    }
    }

function bonus() {
    document.getElementById('prot').style.display = 'none';
    galbe = Math.floor(1.1 * galbe);
}